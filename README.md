
Tecnologias Ópticas e Fotónica 
aplicadas as 

======

This is a set of Jupyter notebooks (running python) for use in a Quantum Mechanics class. 
The material is based on (and in sequence with) Mark Beck's book "Quantum Mechanics: Theory and Experiment" 
but the notebooks can be used independently and with or without a QM textbook.

Run interactive version: [![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/mniehus%2FQMlabs/master)

## Installation:

The notebooks make use of the [QuTiP package](http://qutip.org) 

## Acknowledgement:
This is based on pre
